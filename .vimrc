" best to put this on the first line
set nocompatible
set encoding=utf-8

""""""""""""""""""""""""""""""""""""""""""""""""
" Functionality
""""""""""""""""""""""""""""""""""""""""""""""""
" set console title to open file name
set title
" ignore case during searches unless capitals are used
set ignorecase smartcase
" BASH-style tab completion for filenames
set wildmenu wildmode=longest:full
" allow pasting from outside vim, disabling auto-indent and others
set pastetoggle=<F2>
" disables the bell noise and removes any associated delay
set noerrorbells visualbell t_vb=
" allow switching files and buffers without saving
"set hidden
" allow backspacing over everything
set backspace=indent,eol,start
" don't put cursor at the start of the line unneccessarily
"set nostartofline
" enable folding by indentation level by default
"set foldmethod=indent foldminlines=3 foldlevel=99
" automatically insert leading comment characters when pressing <enter> in insert mode
"set formatoptions+=r
" don't insert leading comment characters when pressing `o` or `O` in normal mode
"set formatoptions-=o
" keep 50 commands in history
set history=50
" swap file directories
set backupdir=~/.vim/backup,.,~
set directory=~/.vim/backup,.,~

"""""""""""""""""""""""""""""""""""""""""""""""
" window split
"""""""""""""""""""""""""""""""""""""""""""""""
" always split below and right
set splitbelow
set splitright
" split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"""""""""""""""""""""""""""""""""""""""""""""""
" Folding
"""""""""""""""""""""""""""""""""""""""""""""""
" Enable folding
set foldmethod=indent
set foldlevel=99
" Enable folding with the spacebar
nnoremap <space> za

"""""""""""""""""""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""""""""""""""""""
" Pathogen
"call pathogen#infect()
" syntax on
"filetype plugin indent on
" YouCompleteMe path flags script
"let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf_for_c.py"
"let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf_for_cpp.py"

"""""""""""""""""""""""""""""""""""""""""""""""
" Vundle
"""""""""""""""""""""""""""""""""""""""""""""""

filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)

" To properly fold python code
Plugin 'tmhedberg/SimpylFold'

" Better autoidentetion
Plugin 'vim-scripts/indentpython.vim'

" Auto-complete plugin
Bundle 'Valloric/YouCompleteMe'

" Syntax checking/highlighting
Plugin 'vim-syntastic/syntastic'

" PEP8 checking
Plugin 'nvie/vim-flake8'

" Color schemes
Plugin 'jnurmine/Zenburn'
Plugin 'altercation/vim-colors-solarized'

" File Browser
Plugin 'scrooloose/nerdtree'
" Start NERDTree automatically
autocmd vimenter * NERDTree

" Git support for NERDTree
Plugin 'Xuyuanp/nerdtree-git-plugin'

" Powerline
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}

" Super searching
Plugin 'kien/ctrlp.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required


"""""""""""""""""""""""""""""""""""""""""""""""
" Completion Menu
"""""""""""""""""""""""""""""""""""""""""""""""
" Remove stupid preview
set completeopt-=preview

""""""""""""""""""""""""""""""""""""""""""""""""
" Indenting
""""""""""""""""""""""""""""""""""""""""""""""""
" auto-indent
"set noautoindent smartindent

" Python identation
au BufNewFile,BufRead *.py
  \set tabstop=4
  \set softtabstop=4
  \set shiftwidth=4
  \set textwidth=79
  \set expandtab
  \set autoindent
  \set fileformat=unix

" Full stack 
au BufNewFile,BufRead *.js,*.html,*.css 
  \set tabstop=2
  \ softtabstop=2
  \ shiftwidth=2

" 2-space indentation; eight-character-wide tabs
set expandtab softtabstop=2 tabstop=8 shiftwidth=2

""""""""""""""""""""""""""""""""""""""""""""""""
" Display
""""""""""""""""""""""""""""""""""""""""""""""""
" show command being entered
set showcmd
" show line numbers
set number
" do not wrap long lines of text
set nowrap
" always show cursor
set ruler
" highlight the cursor's line
" set cursorline
" highlight the cursor's column
" set cursorcolumn
" flash matching bracket on insert
set showmatch
" search as you type
set incsearch
" global search/replace by default
set gdefault
" highlight matched search pattern
set hlsearch
" keep cursor away from vertical edge of terminal
set scrolloff=1
" always display the status bar
set laststatus=2
" 10ms multi-byte keycode timeout avoids input delay
set ttimeoutlen=10

"""""""""""""""""""""""""""""""""""""""""""""""
" Whitespaces 
"""""""""""""""""""""""""""""""""""""""""""""""
" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Flagging unnecessary whitespace 
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

" use :set list! to toggle visible whitespace on/off
set listchars=tab:\¦—,nbsp:\␣,trail:\·,eol:\↵,extends:\»,precedes:\«

"""""""""""""""""""""""""""""""""""""""""""""""
" YouCompleteMe settings
"""""""""""""""""""""""""""""""""""""""""""""""
let g:ycm_autoclose_preview_window_after_completion=1
let mapleader=" "
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"""""""""""""""""""""""""""""""""""""""""""""""
" Let's make pretty highlighting
"""""""""""""""""""""""""""""""""""""""""""""""
let python_highlight_all=1
syntax on
"colorscheme solarized
"set background=dark
" Automatically select which scheme to use
if has('gui_running')
  set background=dark
  colorscheme solarized
else
  colorscheme zenburn
  set background=light
endif

" Change colorscheme
call togglebg#map("<F5>")

